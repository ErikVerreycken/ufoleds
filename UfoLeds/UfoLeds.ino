int timeOut = 30;                               // Set the delay in ms
int fadeAmount = 5;                             // how many points to fade the LED by
int MAX = 255;                                  // Maximum brightness (255 = 100 %)
int MIN = 0;                                    // Minimum brightness (0 = 0%)

int leds = 6;                                   // The amount of leds
int led[] = {3, 5, 6, 9, 10, 11};           // The pin that the LED is attached to
int bright[] = {0, 42, 85, 127, 170, 212}; // how bright the LED is
int bright2[] = {0, 42, 85, 127, 170, 212}; // how bright the LED should be




void setup()  { 
  for (int i = 0; i< leds; i++){              // For all leds
    pinMode(led[i], OUTPUT);                  // Set as output
  }
} 

void loop()  { 
  
  
  for (int i = 0; i< leds; i++){              // For all leds
    if (bright[i] = bright2[i]){
      bright2[i] = random(MIN, MAX);
    } else{
      bright[i] > bright2[i] ? bright[i]-- : bright[i]++;
    }
    analogWrite(led[i], bright[i]);
  }
  // wait for 30 milliseconds to see the dimming effect    
  delay(timeOut);                            
}

